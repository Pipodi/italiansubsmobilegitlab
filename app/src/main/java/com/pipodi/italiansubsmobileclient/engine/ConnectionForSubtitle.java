package com.pipodi.italiansubsmobileclient.engine;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.utils.Constants;

import java.io.File;

/**
 * Created by Alex on 02/04/2016.
 */
public class ConnectionForSubtitle extends AsyncTask<String, String, String> {

    private int id;
    private String fileName;
    private String directory;

    public ConnectionForSubtitle(int id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        SharedPreferences preferences = MainActivity.context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        String defaultExternalPublicDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        this.directory = preferences.getString("DOWNLOADDIR", null).replace(defaultExternalPublicDir, "");
        File direct = new File(this.directory);
        if (!direct.exists()) {
            direct.mkdirs();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        DownloadManager mgr = (DownloadManager) MainActivity.context
                .getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri
                .parse("https://api.italiansubs.net/api/rest/subtitles/download?subtitle_id="
                        + this.id
                        + "&authcode="
                        + "0a7623231022fde8b519d5f6d3084700"
                        + "&apikey="
                        + Constants.APIKey);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle("Download sottotitoli.")
                .setDescription(fileName)
                .setDestinationInExternalPublicDir(directory,
                        fileName + ".zip");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        Log.i("Cookie", LoginVariables.cookieString);
        request.addRequestHeader("Cookie", LoginVariables.cookieString);
        mgr.enqueue(request);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(MainActivity.context, "Il file verrà scaricato nella cartella"
                + directory, Toast.LENGTH_LONG).show();
    }
}
