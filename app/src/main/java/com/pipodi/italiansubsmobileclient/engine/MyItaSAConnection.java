package com.pipodi.italiansubsmobileclient.engine;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.adapters.SubtitlesListAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Subtitle;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 30/04/2016.
 */
public class MyItaSAConnection extends AsyncTask<String, String, String> {

    private List<SubtitleInterface> items;
    private ListView listView;
    private Context context;
    private LayoutInflater inflater;
    private ProgressBar progress;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout refreshLayout;
    private Fragment fragment;
    private boolean error;

    public MyItaSAConnection(ListView view, Context context,
                             LayoutInflater inflater, SwipeRefreshLayout refreshing, ProgressBar progress) {
        this.refreshLayout = refreshing;
        this.context = context;
        this.items = new ArrayList<>();
        this.inflater = inflater;
        this.listView = view;
        this.progress = progress;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!this.refreshLayout.isRefreshing()) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        if (params[0].equals("")) {
            return "";
        }
        Document doc;
        doc = Connection
                .connectToAPIURL("https://api.italiansubs.net/api/rest/myitasa/lastsubtitles?authcode="
                        + User.getInstance().getUserAuthCode()
                        + "&apikey=ce0a811540244ad5267785731a5c37a0", context);
        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
        } else if (doc != null && TempStorage.favouriteShows.size() > 0){
            Iterator iter = XMLIterator.parseXMLTree(2, doc);
            while (iter.hasNext()) {
                Element item = (Element) iter.next();
                String full_name = item.getChild("name").getText();
                String version = item.getChild("version").getText();
                int sub_id = Integer.parseInt(item.getChild("id").getText());
                String episode = "";
                for (String s : TempStorage.favouriteShows) {
                    if (full_name.toLowerCase().contains(s.toLowerCase())) {
                        episode = full_name.toLowerCase().replace(s.toLowerCase(), "");
                    }
                }
                full_name = full_name.replace(episode, "");
                SubtitleInterface sub = new Subtitle(sub_id, full_name, episode, version);
                this.items.add(sub);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        this.progress.setVisibility(View.INVISIBLE);
        if (this.refreshLayout.isRefreshing()) {
            this.refreshLayout.setRefreshing(false);
        }
        if (result != null && result.equals("")) {
            this.items = TempStorage.downloadedMyItaSA;
        }
        Constants.myItaSADownloaded = true;
        TempStorage.downloadedMyItaSA = this.items;
        SubtitlesListAdapter adapter = new SubtitlesListAdapter(context,
                inflater, items);
        listView.setAdapter(adapter);

        this.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(true);
                new MyItaSAConnection(listView, context, inflater, refreshLayout, progress).execute("https://api.italiansubs.net/api/rest/myitasa/lastsubtitles?authcode="
                        + User.getInstance().getUserAuthCode()
                        + "&apikey=ce0a811540244ad5267785731a5c37a0");
            }
        });
    }
}
