package com.pipodi.italiansubsmobileclient.engine;

import android.os.AsyncTask;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.NewsViewerActivity;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class MyItaSAUpdateConnection extends AsyncTask<Integer, Void, Integer> {

    private ImageButton button;
    private String status;
    private boolean isAdding;
    private List<String> showList;
    private boolean error;

    public MyItaSAUpdateConnection(ImageButton button) {
        super();
        this.button = button;
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        Document doc;
        if (params[1] == 0) {
            this.isAdding = true;
            doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/myitasa/addShowToPref?show_id=" + params[0]
                    + "&authcode=" + User.getInstance().getUserAuthCode() + "&apikey=" + Constants.APIKey, NewsViewerActivity.context);
        } else {
            this.isAdding = false;
            doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/myitasa/removeShowFromPref?show_id=" + params[0]
                    + "&authcode=" + User.getInstance().getUserAuthCode() + "&apikey=" + Constants.APIKey, NewsViewerActivity.context);
        }
        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
            return null;
        }
        Iterator iterator = doc.getContent().iterator();
        while (iterator.hasNext()) {
            Element item = (Element) iterator.next();
            status = item.getChild("status").getText();
        }

        if (this.status.equals("success")) {
            this.showList = LoginVariables.getFavouriteShows();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        if (this.error) {
            Toast.makeText(NewsViewerActivity.context, "Errore durante l'aggiornamento del MyItaSA. Riprovare più tardi.", Toast.LENGTH_LONG).show();
        } else {
            if (this.status.equals("fail")) {
                if (this.isAdding) {
                    Toast.makeText(NewsViewerActivity.context, "Errore durante l'aggiunta della serie al MyItaSA.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(NewsViewerActivity.context, "Errore durante la rimozione della serie al MyItaSA.", Toast.LENGTH_LONG).show();
                }
            } else {
                TempStorage.favouriteShows = this.showList;
                if (this.isAdding) {
                    Toast.makeText(NewsViewerActivity.context, "Serie aggiunta correttamente al MyItaSA.", Toast.LENGTH_LONG).show();
                    button.setBackgroundDrawable(NewsViewerActivity.context.getResources().getDrawable(R.drawable.pref_rem));
                } else {
                    Toast.makeText(NewsViewerActivity.context, "Serie rimossa correttamente dal MyItaSA.", Toast.LENGTH_LONG).show();
                    button.setBackgroundDrawable(NewsViewerActivity.context.getResources().getDrawable(R.drawable.pref_add));
                }
            }
        }
    }
}
