package com.pipodi.italiansubsmobileclient.engine;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pipodi.italiansubsmobileclient.activities.NewsViewerActivity;
import com.pipodi.italiansubsmobileclient.adapters.NewsAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.ImageConnection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.News;
import com.pipodi.italiansubsmobileclient.model.iterfaces.NewsInterface;
import com.pipodi.italiansubsmobileclient.utils.DateFormatChanger;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 22/03/2016.
 */
public class NewsEngine extends AsyncTask<String, String, String> {

    private Context context;
    private ListView newsListView;
    private List<NewsInterface> items;
    private NewsAdapter newsAdapter;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout refreshLayout;

    public NewsEngine(Context context, ListView newsListView, SwipeRefreshLayout refreshLayout) {
        this.context = context;
        this.newsListView = newsListView;
        this.items = new ArrayList<>();
        this.refreshLayout = refreshLayout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!this.refreshLayout.isRefreshing()) {
            this.progressDialog = ProgressDialog.show(context, "Caricamento news...", "Attendere prego...", true);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        if (params[0].equals("")) {
            return "";
        } else {
            Document document = Connection.connectToAPIURL(params[0], this.context);
            Iterator documentIterator = null;
            if (document != null && LoginVariables.isRequestValid(document)) {
                documentIterator = XMLIterator.parseXMLTree(2, document);
            } else {
                return null;
            }
            while (documentIterator.hasNext()) {
                Element item = (Element) documentIterator.next();
                int showId = Integer.parseInt(item.getChild("show_id").getText());
                int showCategory = Integer.parseInt(item.getChild("category").getText());
                if (showId != 0 && showCategory == 16) {
                    String title = item.getChild("show_name").getText();
                    Iterator temp = item.getChild("episodes").getChildren().iterator();
                    String episode = "";
                    if (!item.getChild("special").getText().equals("")) {
                        episode += (item.getChild("special").getText() + " ").toUpperCase();
                    }
                    while (temp.hasNext()) {
                        episode += ((Element) temp.next()).getText() + " ";
                    }
                    String release = item.getChild("submit_date").getText();
                    String photo_url = item.getChild("image").getText();
                    int news_id = Integer.parseInt(item.getChild("id").getText());
                    release = DateFormatChanger.changeDateFormat(release);
                    Bitmap photo = ImageConnection.getPhotoFromURL(photo_url);
                    NewsInterface news = new News(showId, news_id, title, episode, release, photo, photo_url);
                    this.items.add(news);
                }
                TempStorage.downloadedNews = this.items;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (!this.refreshLayout.isRefreshing() && (this.progressDialog != null && this.progressDialog.isShowing())) {
            this.progressDialog.dismiss();
        } else {
            this.refreshLayout.setRefreshing(false);
        }
        if (s != null && s.equals("")) {
            this.items = TempStorage.downloadedNews;
        }
        this.newsAdapter = new NewsAdapter(this.items, this.context);
        this.newsListView.setAdapter(this.newsAdapter);
        this.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(true);
                new NewsEngine(context, newsListView, refreshLayout).execute("https://api.italiansubs.net/api/rest/news?&apikey=ce0a811540244ad5267785731a5c37a0");
            }
        });

        this.newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                NewsInterface temp = items.get(position);
                String current_title = temp.getTitle();
                String current_episode = temp.getEpisode();
                String current_release = temp.getReleaseDate();
                Bitmap current_photo = temp.getPhoto();
                int current_showID = temp.getID();
                int current_newsID = temp.getNewsID();
                boolean isFavourite = temp.isFavourite();
                String current_photoURL = temp.getPhotoURL();
                Log.i("PHOTOURL", current_photoURL);
                Intent openingIntent = new Intent(context,
                        NewsViewerActivity.class);
                openingIntent.putExtra("PHOTOURL", current_photoURL);
                openingIntent.putExtra("TITLE", current_title);
                openingIntent.putExtra("EPISODE", current_episode);
                openingIntent.putExtra("RELEASE", current_release);
                openingIntent.putExtra("PHOTO", current_photo);
                openingIntent.putExtra("FAVOURITE", isFavourite);
                TempStorage.currentNewsID = current_newsID;
                TempStorage.currentShowID = current_showID;
                openingIntent.putExtra("ID", current_showID);
                openingIntent.putExtra("NEWSID", current_newsID);
                context.startActivity(openingIntent);
            }
        });
    }
}
