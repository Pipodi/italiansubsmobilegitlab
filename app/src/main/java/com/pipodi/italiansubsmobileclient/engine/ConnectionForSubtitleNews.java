package com.pipodi.italiansubsmobileclient.engine;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.NewsViewerActivity;
import com.pipodi.italiansubsmobileclient.adapters.SubtitlesListAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Subtitle;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class ConnectionForSubtitleNews extends AsyncTask<String, String, String> {

    private static String title;
    private static String episodeString;
    private static String[] episode;
    private ListView listView;
    private LayoutInflater inflater;
    private List<SubtitleInterface> items = new ArrayList<>();
    private boolean error = false;
    private ProgressBar progressBar;

    public ConnectionForSubtitleNews(String title, String episodeString, ListView listView, LayoutInflater inflater, ProgressBar progressBar) {
        this.title = title;
        this.listView = listView;
        this.inflater = inflater;
        this.episodeString = episodeString;
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressBar.setVisibility(View.VISIBLE);
        this.episode = episodeString.split(" ");
    }

    @Override
    protected String doInBackground(String... params) throws NullPointerException {
        Document doc = Connection
                .connectToAPIURL("https://api.italiansubs.net/api/rest/subtitles/search?q="
                        + tvSeriesTitleChecker(this.title)
                        + "&apikey=ce0a811540244ad5267785731a5c37a0", NewsViewerActivity.context);
        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
        } else {
            Iterator iter = XMLIterator.parseXMLTree(2, doc);
            while (iter.hasNext()) {
                Element item = (Element) iter.next();
                String name = item.getChild("name").getText();
                String show_name = item.getChild("show_name").getText();
                String version = item.getChild("version").getText();
                String episode = episodeNumberParser(item.getChild("name").getText(), show_name);
                int sub_id = Integer.parseInt(item.getChild("id").getText());
                int i = 0;
                for (String string : this.episode) {
                    if (name.contains(this.episode[i])) {
                        SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                episode, version);
                        this.items.add(sub);
                    }
                    i++;
                }
            }
            if (this.items.size() == 0) {
                this.error = true;
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        this.progressBar.setVisibility(View.GONE);
        if (!error) {
            SubtitlesListAdapter adapter = new SubtitlesListAdapter(NewsViewerActivity.context,
                    inflater, items);
            listView.setAdapter(adapter);
        } else {
            Toast.makeText(NewsViewerActivity.context, "Problema durante la ricerca dei sottotitoli. Riprova più tardi.", Toast.LENGTH_LONG).show();
        }
    }

    private String episodeNumberParser(String str, String name) {
        return str.replace(name + " ", "");
    }

    private String tvSeriesTitleChecker(String title) {
        String temp;
        temp = title.replace("'", "%27");
        temp = temp.replace(" ", "%20");
        return temp;
    }
}
