package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.ImgViewerActivity;
import com.pipodi.italiansubsmobileclient.activities.ProfileViewerActivity;
import com.pipodi.italiansubsmobileclient.model.ForumOption;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.PostInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.TopicInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 13/04/2016.
 */
public class ForumAdapter extends BaseAdapter {

    private Context context;
    private List<ForumObjInterface> items;
    private LinearLayout postContent;

    public ForumAdapter(Context context, List<ForumObjInterface> items) {
        this.context = context;
        this.items = items;
    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ForumObjInterface forumObj = this.items.get(position);
        switch (forumObj.getType()) {
            case FORUM:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.forum_list_item, parent, false);
                ForumInterface forum = (ForumInterface) forumObj;
                TextView forumTitle = (TextView) convertView.findViewById(R.id.forumTitle);
                forumTitle.setText(forum.getName());
                ImageView forumIcon = (ImageView) convertView.findViewById(R.id.forumIcon);
                if (forum.getChild() != null) {
                    Map<String, Object> forumChilds = forum.getChild();
                    if (forumChilds.size() != 0) {
                        Iterator<Map.Entry<String, Object>> tempIterator = forumChilds.entrySet().iterator();
                        while (tempIterator.hasNext()) {
                            Map.Entry<String, Object> currentForum = tempIterator.next();
                            Object[] currentChilds = (Object[]) currentForum.getValue();
                            for (int i = 0; i < currentChilds.length; i++) {
                                Map<String, Object> parsedChild = (Map<String, Object>) currentChilds[i];
                                Iterator<Map.Entry<String, Object>> childIterator = parsedChild.entrySet().iterator();
                                while (childIterator.hasNext()) {
                                    Map.Entry<String, Object> currentChild = childIterator.next();
                                    if (currentChild.getKey().equals("new_post")) {
                                        if ((Boolean) currentChild.getValue() == true) {
                                            forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.unread_forum));
                                            break;
                                        } else {
                                            forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.read_forum));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (forum.isRead()) {
                            forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.unread_forum));
                        } else {
                            forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.read_forum));
                        }
                    }
                } else {
                    if (forum.isRead()) {
                        forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.unread_forum));
                    } else {
                        forumIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.read_forum));
                    }
                }
                break;
            case TOPIC:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.topic_list_item, parent, false);
                TopicInterface topic = (TopicInterface) forumObj;
                TextView topicTitle = (TextView) convertView.findViewById(R.id.topicTitle);
                TextView topicAuthor = (TextView) convertView.findViewById(R.id.topicAuthor);
                topicTitle.setText(topic.getTitle());
                if (!topic.isUnreadTopicType()) {
                    topicAuthor.setText(Html.fromHtml("<b>Autore: </b>" + topic.getTopicAuthor()));
                } else {
                    topicAuthor.setText(Html.fromHtml("<b>Ultimo post: </b>" + topic.getTopicAuthor()));
                }
                ImageView topicIcon = (ImageView) convertView.findViewById(R.id.topicIcon);
                if (topic.isRead()) {
                    topicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.on));
                } else {
                    topicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.off));
                }
                break;
            case STICKY_TOPIC:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.topic_list_item, parent, false);
                TopicInterface stickyTopic = (TopicInterface) forumObj;
                TextView stickyTopicTitle = (TextView) convertView.findViewById(R.id.topicTitle);
                TextView stickyTopicAuthor = (TextView) convertView.findViewById(R.id.topicAuthor);
                stickyTopicTitle.setText(stickyTopic.getTitle());
                stickyTopicAuthor.setText(Html.fromHtml("<b>Autore: </b>" + stickyTopic.getTopicAuthor()));
                ImageView stickyTopicIcon = (ImageView) convertView.findViewById(R.id.topicIcon);
                if (stickyTopic.isRead()) {
                    stickyTopicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.sticky_off));
                } else {
                    stickyTopicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.sticky_on));
                }
                break;
            case POST:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.post_item_layout, parent, false);
                postContent = (LinearLayout) convertView.findViewById(R.id.postContent);
                final PostInterface post = (PostInterface) forumObj;
                TextView postAuthor = (TextView) convertView.findViewById(R.id.postAuthor);
                TextView postDate = (TextView) convertView.findViewById(R.id.postDate);
                RoundedImageView postAvatar = (RoundedImageView) convertView.findViewById(R.id.postAvatar);
                Picasso.with(context).load(post.getAvatarURL()).fit().placeholder(R.drawable.profile).into(postAvatar);
                postAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ProfileViewerActivity.class);
                        intent.putExtra("USERID", post.getPostAuthorID());
                        intent.putExtra("USERNAME", post.getPostAuthor());
                        context.startActivity(intent);
                    }
                });
                postAuthor.setText(post.getPostAuthor());
                postDate.setText(post.getPostDate().toString());
                setPostContent(post);
                break;
            case UNREAD_REPLIES:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.topic_list_item, parent, false);
                TopicInterface unreadReplies = (TopicInterface) forumObj;
                TextView unreadTitle = (TextView) convertView.findViewById(R.id.topicTitle);
                TextView unreadAuthor = (TextView) convertView.findViewById(R.id.topicAuthor);
                unreadTitle.setText(unreadReplies.getTitle());
                if (!unreadReplies.isUnreadTopicType()) {
                    unreadAuthor.setText(Html.fromHtml("<b>Autore: </b>" + unreadReplies.getTopicAuthor()));
                } else {
                    unreadAuthor.setText(Html.fromHtml("<b>Ultimo post: </b>" + unreadReplies.getTopicAuthor()));
                }
                ImageView unreadIcon = (ImageView) convertView.findViewById(R.id.topicIcon);
                if (unreadReplies.isRead()) {
                    unreadIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.on));
                } else {
                    unreadIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.off));
                }
                break;
            case UNREAD_TOPICS:
                convertView = LayoutInflater.from(this.context).inflate(R.layout.topic_list_item, parent, false);
                TopicInterface unreadTopic = (TopicInterface) forumObj;
                TextView unreadTopicTitle = (TextView) convertView.findViewById(R.id.topicTitle);
                TextView unreadTopicAuthor = (TextView) convertView.findViewById(R.id.topicAuthor);
                unreadTopicTitle.setText(unreadTopic.getTitle());
                if (!unreadTopic.isUnreadTopicType()) {
                    unreadTopicAuthor.setText(Html.fromHtml("<b>Autore: </b>" + unreadTopic.getTopicAuthor()));
                } else {
                    unreadTopicAuthor.setText(Html.fromHtml("<b>Ultimo post: </b>" + unreadTopic.getTopicAuthor()));
                }
                ImageView unreadTopicIcon = (ImageView) convertView.findViewById(R.id.topicIcon);
                if (unreadTopic.isRead()) {
                    unreadTopicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.on));
                } else {
                    unreadTopicIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.off));
                }
                break;
            case OPTION:
                convertView = LayoutInflater.from(context).inflate(R.layout.forum_option_layout, parent, false);
                ForumOption forumOption = (ForumOption) forumObj;
                TextView optionText = (TextView) convertView.findViewById(R.id.forumOptionText);
                optionText.setText(forumOption.getText());
                break;
        }
        return convertView;
    }

    private void setPostContent(PostInterface post) {
        List<View> postItems = new ArrayList<>();
        if (!post.getPostText().contains("<spoiler>")) {
            /**
             * Normal post handling
             */
            if (post.getPostText().contains("<img>")) {
                /**
                 * Normal post with images
                 */
                final List<String> textList = Arrays.asList(post.getPostText().split("<img>"));
                for (int i = 0; i < textList.size(); i++) {
                    postItems.add(new View(context));
                }
                for (int i = 0; i < textList.size(); i += 2) {
                    if (!textList.get(i).equals(" ")) {
                        TextView textView = new TextView(context);
                        textView.setMovementMethod(LinkMovementMethod.getInstance());
                        textView.setText(Html.fromHtml(textList.get(i)));
                        postItems.set(i, textView);
                    }
                }
                for (int i = 1; i < textList.size(); i += 2) {
                    ImageView imageView = new ImageView(context);
                    final int finalI = i;
                    int imageHeight = 0;
                    int imageWidth = 0;
                    float metrics = context.getResources().getDisplayMetrics().density;
                    if (metrics == 3.0) {
                        Log.i("DYSPLAYSIZE: ", "xxhdpi");
                        imageHeight = 350;
                        imageWidth = 450;
                    } else if (metrics == 2.0) {
                        Log.i("DYSPLAYSIZE: ", "xhdpi");
                        imageHeight = 320;
                        imageWidth = 420;
                    } else if (metrics == 1.5) {
                        Log.i("DYSPLAYSIZE: ", "hdpi");
                        imageHeight = 290;
                        imageWidth = 390;
                    }
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageHeight));
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, ImgViewerActivity.class);
                            intent.putExtra("LINK", textList.get(finalI));
                            context.startActivity(intent);
                        }
                    });
                    Glide.with(context).load(textList.get(i)).placeholder(R.drawable.placeholder).into(imageView);
                    postItems.set(i, imageView);
                }
                for (View view : postItems) {
                    this.postContent.addView(view);
                }
            } else {
                /**
                 * Normal post without images
                 */
                TextView textView = new TextView(context);
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setText(Html.fromHtml(post.getPostText()));
                this.postContent.addView(textView);
            }
        } else {
            /**
             * Post with spoilers
             */
            List<String> textList = Arrays.asList(post.getPostText().split("<spoiler>"));
            for (int i = 0; i < textList.size(); i++) {
                postItems.add(new View(context));
            }
            for (int i = 0; i < textList.size(); i += 2) {
                if (!textList.get(i).equals(" ")) {
                    if (textList.get(i).contains("<img>")) {
                        final List<String> innerList = Arrays.asList(textList.get(i).split("<img>"));
                        final List<View> itemsList = new ArrayList<>(innerList.size());
                        for (int y = 0; y < innerList.size(); y++) {
                            itemsList.add(new View(context));
                        }
                        for (int y = 0; y < innerList.size(); y += 2) {
                            if (!innerList.get(y).equals(" ")) {
                                TextView textView = new TextView(context);
                                textView.setMovementMethod(LinkMovementMethod.getInstance());
                                textView.setText(Html.fromHtml(innerList.get(y).replace("[/SPOILER]", "")));
                                itemsList.set(y, textView);
                            }
                        }
                        for (int y = 1; y < innerList.size(); y += 2) {
                            ImageView imageView = new ImageView(context);
                            final int finalI = y;
                            int imageHeight = 0;
                            int imageWidth = 0;
                            float metrics = context.getResources().getDisplayMetrics().density;
                            if (metrics == 3.0) {
                                Log.i("DYSPLAYSIZE: ", "xxhdpi");
                                imageHeight = 350;
                                imageWidth = 450;
                            } else if (metrics == 2.0) {
                                Log.i("DYSPLAYSIZE: ", "xhdpi");
                                imageHeight = 320;
                                imageWidth = 420;
                            } else if (metrics == 1.5) {
                                Log.i("DYSPLAYSIZE: ", "hdpi");
                                imageHeight = 290;
                                imageWidth = 390;
                            }
                            imageView.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageHeight));
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ImgViewerActivity.class);
                                    intent.putExtra("LINK", innerList.get(finalI));
                                    context.startActivity(intent);
                                }
                            });
                            Glide.with(context).load(innerList.get(y)).placeholder(R.drawable.placeholder).into(imageView);
                            itemsList.set(y, imageView);
                        }
                        postItems.addAll(i, itemsList);
                    } else {
                        TextView textView = new TextView(context);
                        textView.setMovementMethod(LinkMovementMethod.getInstance());
                        textView.setText(Html.fromHtml(textList.get(i).replace("[/SPOILER]", "")));
                        postItems.set(i, textView);
                    }
                }
            }
            for (int i = 1; i < textList.size(); i += 2) {
                Button button = new Button(context);
                button.setText("SPOILER");
                int buttonHeight = 0;
                float metrics = context.getResources().getDisplayMetrics().density;
                if (metrics == 3.0) {
                    Log.i("DYSPLAYSIZE: ", "xxhdpi");
                    buttonHeight = 140;
                } else if (metrics == 2.0) {
                    Log.i("DYSPLAYSIZE: ", "xhdpi");
                    buttonHeight = 120;
                } else if (metrics == 1.5) {
                    Log.i("DYSPLAYSIZE: ", "hdpi");
                    buttonHeight = 100;
                }
                button.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, buttonHeight));
                if (textList.get(i).contains("<img>")) {
                    final List<String> splittedList = Arrays.asList(textList.get(i).split("<img>"));
                    final List<View> spoilerItems = new ArrayList<>(splittedList.size());
                    for (int y = 0; y < splittedList.size(); y++) {
                        spoilerItems.add(new View(context));
                    }
                    spoilerItems.add(0, button);
                    for (int y = 1; y < splittedList.size(); y += 2) {
                        ImageView imageView = new ImageView(context);
                        final int finalI = y;
                        imageView.setVisibility(View.GONE);
                        int imageHeight = 0;
                        int imageWidth = 0;
                        if (metrics == 3.0) {
                            Log.i("DYSPLAYSIZE: ", "xxhdpi");
                            imageHeight = 350;
                            imageWidth = 450;
                        } else if (metrics == 2.0) {
                            Log.i("DYSPLAYSIZE: ", "xhdpi");
                            imageHeight = 320;
                            imageWidth = 420;
                        } else if (metrics == 1.5) {
                            Log.i("DYSPLAYSIZE: ", "hdpi");
                            imageHeight = 290;
                            imageWidth = 390;
                        }
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageHeight));
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(context, ImgViewerActivity.class);
                                intent.putExtra("LINK", splittedList.get(finalI));
                                context.startActivity(intent);
                            }
                        });
                        final boolean[] visible = {false};
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!visible[0]) {
                                    for (View view : spoilerItems) {
                                        if (!view.getClass().equals(Button.class)) {
                                            view.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    visible[0] = true;
                                } else {
                                    for (View view : spoilerItems) {
                                        if (!view.getClass().equals(Button.class)) {
                                            view.setVisibility(View.GONE);
                                        }
                                    }
                                    visible[0] = false;
                                }
                            }
                        });
                        Glide.with(context).load(splittedList.get(finalI)).placeholder(R.drawable.placeholder).into(imageView);
                        spoilerItems.set(y + 1, imageView);
                        if (!splittedList.get(y - 1).equals("") || !splittedList.get(y - 1).equals(" ")) {
                            TextView textView = new TextView(context);
                            textView.setVisibility(View.GONE);
                            textView.setMovementMethod(LinkMovementMethod.getInstance());
                            textView.setText(Html.fromHtml(splittedList.get(y - 1).replace("[SPOILER]", "")));
                            spoilerItems.set(y, textView);
                        }
                    }
                    postItems.addAll(i + 1, spoilerItems);
                } else {
                    final TextView textView = new TextView(context);
                    textView.setTypeface(null, Typeface.ITALIC);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setText(Html.fromHtml(textList.get(i).replace("[/SPOILER]", "")));
                    textView.setVisibility(View.GONE);
                    final boolean[] visible = {false};
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!visible[0]) {
                                textView.setVisibility(View.VISIBLE);
                                visible[0] = true;
                            } else {
                                textView.setVisibility(View.GONE);
                                visible[0] = false;
                            }
                        }
                    });
                    postItems.set(i, button);
                    int temp = i + 1;
                    postItems.add(temp, textView);
                }
            }
            for (View view : postItems) {
                if (!view.getClass().equals(View.class)) {
                    this.postContent.addView(view);
                }
            }
        }
    }
}
