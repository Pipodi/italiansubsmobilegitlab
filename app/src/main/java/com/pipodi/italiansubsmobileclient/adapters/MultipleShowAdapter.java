package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 12/04/2016.
 */
public class MultipleShowAdapter extends BaseAdapter {

    private List<ShowInterface> list = new ArrayList<>();
    private Context context;

    public MultipleShowAdapter(List<ShowInterface> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int i) {
        return this.list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ShowInterface item = this.list.get(i);
        view = LayoutInflater.from(context).inflate(R.layout.show_item_list_layout, viewGroup, false);
        TextView showName = (TextView) view.findViewById(R.id.showName);
        showName.setText(item.getShowName());
        return view;
    }
}
