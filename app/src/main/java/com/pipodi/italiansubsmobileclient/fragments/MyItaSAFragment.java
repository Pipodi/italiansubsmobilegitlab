package com.pipodi.italiansubsmobileclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.MyItaSAConnection;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

/**
 * Created by Alex on 30/04/2016.
 */
public class MyItaSAFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View generalView = inflater.inflate(R.layout.myitasa_fragment_layout, container, false);
        final ListView myItaSAList = (ListView) generalView.findViewById(R.id.myitasa_list);
        ProgressBar progressBar = (ProgressBar) generalView.findViewById(R.id.progressBarMyItaSA);
        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) generalView.findViewById(R.id.refreshLayout);
        myItaSAList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (myItaSAList == null || myItaSAList.getChildCount() == 0) ?
                                0 : myItaSAList.getChildAt(0).getTop();
                refreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }
        });
        if (TempStorage.downloadedMyItaSA.size() > 0) {
            new MyItaSAConnection(myItaSAList, this.context, inflater, refreshLayout, progressBar).execute("");
        } else {
            new MyItaSAConnection(myItaSAList, this.context, inflater, refreshLayout, progressBar).execute("https://api.italiansubs.net/api/rest/myitasa/lastsubtitles?authcode="
                    + User.getInstance().getUserAuthCode()
                    + "&apikey=ce0a811540244ad5267785731a5c37a0");
        }
        return generalView;
    }
}
