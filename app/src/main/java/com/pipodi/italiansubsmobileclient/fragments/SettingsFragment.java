package com.pipodi.italiansubsmobileclient.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.adapters.OptionMenuAdapter;
import com.pipodi.italiansubsmobileclient.model.Option;
import com.pipodi.italiansubsmobileclient.model.OptionTypes;
import com.pipodi.italiansubsmobileclient.model.iterfaces.OptionInterface;
import com.pipodi.italiansubsmobileclient.util.IabHelper;
import com.pipodi.italiansubsmobileclient.util.IabResult;
import com.pipodi.italiansubsmobileclient.util.Purchase;
import com.pipodi.italiansubsmobileclient.utils.ChangelogReader;
import com.pipodi.italiansubsmobileclient.utils.Constants;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Alex on 13/04/2016.
 */
public class SettingsFragment extends Fragment {

    private static final int REQUEST_DIRECTORY = 10;
    private static final int REQUEST_PURCHASE = 10001;

    private Context context;
    private SharedPreferences preferences;
    private ListView optionsList;
    private ArrayList<OptionInterface> options;
    private TextView mDirectoryTextView;
    private TextView notificationsVersionsTextView;
    private IabHelper mHelper;
    private AdView mAdView;
    private String base64PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlUv5l8v1lT7J/siwN9NPiItXtotuo1JGKmWw9sa" +
            "fS2wTVXkF/LuaVRWTNBVMp8a8SLKkTsSgGsd+W9FzQ6+tEtljEikmReTfoUSAJ/JKcNju+ZGkiZipLOOHK063Qrqr78o+h" +
            "mRhCyHC8WU4jyL1/GuZCYbHu8yVwVQoTA+QPyYcB/lylFwIWt3zXnNPffR1i4MV4IrSa1ROJgq5BUvT/EmBz+lJVh62cP2YHnhh" +
            "jDnxcQXmBAT7M9PD6YzgtdqfodmD/kQuPzY2ten8/oSLj1u4SqTNx1x6CJi+7udJxyqjzsaCQ2qIhln+unXviI6Ugw8Kt2+kJBeijZhU" +
            "siQYmQIDAQAB";
    private boolean[] notificationIndexes;
    private String[] notificationsPrefs;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View generalView = inflater.inflate(R.layout.settings_fragment_layout, container, false);
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mHelper = new IabHelper(MainActivity.activity, base64PublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("IabHelper", "Problem setting up In-app Billing: " + result);
                }
            }
        });
        this.notificationsPrefs = getResources().getStringArray(R.array.notificationChoices);
        this.notificationIndexes = new boolean[getResources().getStringArray(R.array.notificationChoices).length];
        this.preferences = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        this.optionsList = (ListView) generalView.findViewById(R.id.optionList);
        this.options = new ArrayList<>();
        this.options.add(new Option("Generali", "", OptionTypes.HEADER));
        this.options.add(new Option("Attualmente loggato come:", context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                .getString("USER", ""), OptionTypes.NORMAL));
        this.options.add(new Option("Cartella download", context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                .getString("DOWNLOADDIR", Environment.getExternalStorageDirectory() + "/ItaliansubsMobileSubs"), OptionTypes.DIRECTORY));
        Log.i("DOWNLOADDIR", context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("DOWNLOADDIR", "null"));
        this.options.add(new Option("Inverti tema", "Clicca per invertire il tema dell'applicazione.", OptionTypes.NORMAL));
        this.options.add(new Option("Attiva notifiche", "", OptionTypes.SWITCH));
        this.options.add(new Option("Attiva notifiche per:", context.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE)
                .getString("NOTPREF", "Normale"), OptionTypes.NORMAL));
        this.options.add(new Option("Rimovi pubblicità", "Clicca per rimuovere la pubblicità!", OptionTypes.NORMAL));
        this.options.add(new Option("Informazioni sull'app", "", OptionTypes.HEADER));
        this.options.add(new Option("Pagina Facebook ufficiale", "Metti un like per essere costantemente aggiornato!", OptionTypes.NORMAL));
        this.options.add(new Option("Versione", "v " + pInfo.versionName, OptionTypes.NORMAL));
        OptionMenuAdapter optionMenuAdapter = new OptionMenuAdapter(context, this.options);
        this.optionsList.setAdapter(optionMenuAdapter);
        this.optionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 2:
                        mDirectoryTextView = (TextView) view.findViewById(R.id.optionDescription);
                        Intent directoryIntent = new Intent(context, DirectoryChooserActivity.class);
                        directoryIntent.putExtra(DirectoryChooserActivity.EXTRA_NEW_DIR_NAME,
                                "ItaliansubsMobileSubs");
                        startActivityForResult(directoryIntent, REQUEST_DIRECTORY);
                        break;
                    case 3:
                        if (preferences.getInt("THEME", 0) == 0) {
                            preferences.edit().putInt("THEME", 1).commit();
                        } else {
                            preferences.edit().putInt("THEME", 0).commit();
                        }
                        MainActivity.activity.finish();
                        MainActivity.activity.startActivity(new Intent(MainActivity.activity, MainActivity.activity.getClass()));
                        break;
                    case 5:
                        notificationsVersionsTextView = (TextView) view.findViewById(R.id.optionDescription);
                        String[] indexStringFromPreferences = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE)
                                .getString("INDEXSTRING", "0").split(",");
                        for (String str : indexStringFromPreferences) {
                            if (!str.equals(",")) {
                                notificationIndexes[Integer.parseInt(str)] = true;
                            }
                        }
                        new AlertDialog.Builder(MainActivity.context)
                                .setTitle("Versione:")
                                .setMultiChoiceItems(R.array.notificationChoices, notificationIndexes, new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                        if (b) {
                                            notificationIndexes[i] = true;
                                        } else {
                                            notificationIndexes[i] = false;
                                        }
                                    }
                                }).setPositiveButton("Accetta", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (!checkSelection(notificationIndexes)){
                                    notificationIndexes[0] = true;
                                }
                                StringBuilder indexString = new StringBuilder("");
                                StringBuilder settingsString = new StringBuilder("");
                                for (int y = 0; y < notificationIndexes.length; y++) {
                                    if (notificationIndexes[y] == true) {
                                        indexString.append(y + ",");
                                        settingsString.append(notificationsPrefs[y] + ", ");
                                    }
                                }
                                String finalIndexString = indexString.toString().substring(0, indexString.toString().length() - 1);
                                String finalSettingsString = settingsString.toString().substring(0, settingsString.toString().length() - 2);
                                context.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit()
                                        .putString("INDEXSTRING", finalIndexString).commit();
                                context.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit()
                                        .putString("NOTPREF", finalSettingsString).commit();
                                notificationsVersionsTextView.setText(finalSettingsString);
                            }
                        }).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
                        break;
                    case 6:
                        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
                                = new IabHelper.OnIabPurchaseFinishedListener() {
                            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                                if (result.isFailure()) {
                                    Log.d("IabHelper", "Error purchasing: " + result);
                                    if (result.getResponse() == 7) {
                                        Toast.makeText(context, "Le pubblicità sono già state rimosse!", Toast.LENGTH_LONG).show();
                                        preferences.edit().putBoolean("ADS", false).commit();
                                    }
                                    return;
                                }
                                if (purchase.getSku().equals("remove_ads")) {
                                    Log.d("IabHelper", "Purchased: " + result);
                                }
                            }
                        };
                        mHelper.launchPurchaseFlow(MainActivity.activity, "remove_ads", REQUEST_PURCHASE, mPurchaseFinishedListener);
                        break;
                    case 8:
                        try {
                            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/827359497350602")));
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ItaliansubsMobile")));
                        }
                        break;
                    case 9:
                        ChangelogReader cl = new ChangelogReader(context);
                        Dialog dialog = cl.getFullLogDialog();
                        dialog.setCancelable(true);
                        dialog.show();
                        break;
                }
            }

            private boolean checkSelection(boolean[] notificationIndexes) {
                for (int i = 0; i < notificationIndexes.length; i++){
                    if (notificationIndexes[i] == true){
                        return true;
                    }
                }
                return false;
            }
        });
        return generalView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper.handleActivityResult(requestCode, resultCode, data)) {
            Log.i("OnActivityResult", "Handled by IAB.");
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_DIRECTORY) {
                if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                    mDirectoryTextView
                            .setText(data
                                    .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR));

                    SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit();
                    editor.putBoolean("CHANGED", true).commit();
                    editor.putString("DOWNLOADDIR", data.getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR)).commit();
                }
            }
        }
    }
}
