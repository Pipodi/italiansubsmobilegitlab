package com.pipodi.italiansubsmobileclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.BrowseShowEngine;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

/**
 * Created by pipod on 22/03/2016.
 */
public class BrowseFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View generalView = inflater.inflate(R.layout.browse_fragment_layout, container, false);
        final EditText searchField = (EditText) generalView.findViewById(R.id.showTitle);
        ListView showList = (ListView) generalView.findViewById(R.id.showList);
        ProgressBar progressBarBrowse = (ProgressBar) generalView.findViewById(R.id.loading);
        final BrowseShowEngine browseShowEngine;
        if (TempStorage.showList.size() > 0 && TempStorage.totalShows == TempStorage.showList.size()) {
            browseShowEngine = new BrowseShowEngine(context, showList, progressBarBrowse);
            browseShowEngine.execute("");
        } else {
            browseShowEngine = new BrowseShowEngine(context, showList, progressBarBrowse);
            browseShowEngine.execute("https://api.italiansubs.net/api/rest/shows?apikey=" + Constants.APIKey);
        }
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                browseShowEngine.instantSearch(searchField.getText().toString());
            }
        });
        return generalView;
    }
}
