package com.pipodi.italiansubsmobileclient.utils;

/**
 * Created by Alex on 22/03/2016.
 */
public class DateFormatChanger {

    public static String changeDateFormat(String date) {
        String temp[] = date.split("\\-");
        String hour[] = temp[2].split("\\:");
        String delete = temp[2].substring(2, 17);
        temp[2] = temp[2].replace(delete, "");
        delete = hour[0].substring(0, 3);
        hour[0] = hour[0].replace(delete, "");
        int current_hour = Integer.parseInt(hour[0]);
        current_hour = current_hour + 2;
        if (current_hour > 24) {
            current_hour = current_hour - 24;
            if (Math.floor(Math.log10(current_hour) + 1) == 1) {
                hour[0] = "0" + current_hour;
            } else {
                hour[0] = "" + current_hour;
            }
        }
        delete = hour[2].substring(2, 5);
        hour[2] = hour[2].replace(delete, "");
        return temp[2] + "/" + temp[1] + "/" + temp[0] + " " + hour[0] + ":" + hour[1] + ":" + hour[2];
    }
}
