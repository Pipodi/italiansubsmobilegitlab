package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.CurrentTranslationInterface;

/**
 * Created by pipod on 27/03/2016.
 */
public class CurrentTranslation implements CurrentTranslationInterface {

    private String name;
    private int percent;

    public CurrentTranslation(String name, int percent) {
        this.name = name;
        this.percent = percent;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getPercent() {
        return this.percent;
    }
}
