package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumOptionInterface;

/**
 * Created by Alex on 13/04/2016.
 */
public class ForumOption implements ForumOptionInterface, ForumObjInterface {

    private ForumTypes type;
    private String text;

    public ForumOption(ForumTypes type, String text) {
        this.type = type;
        this.text = text;
    }

    @Override
    public ForumTypes getType() {
        return this.type;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
