package com.pipodi.italiansubsmobileclient.model;

/**
 * Created by Alex on 13/04/2016.
 */
public enum OptionTypes {
    HEADER, NORMAL, DIRECTORY, SWITCH
}
