package com.pipodi.italiansubsmobileclient.model.iterfaces;

import com.pipodi.italiansubsmobileclient.model.ForumTypes;

/**
 * Created by Alex on 13/04/2016.
 */
public interface ForumObjInterface {

    ForumTypes getType();
}
