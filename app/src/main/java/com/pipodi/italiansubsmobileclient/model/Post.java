package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.PostInterface;

/**
 * Created by Alex on 13/04/2016.
 */
public class Post implements PostInterface, ForumObjInterface {

    private String postID;
    private String postText;
    private String postTitle;
    private String postAuthor;
    private String postDate;
    private ForumTypes type;
    private String forumID;
    private String topicID;
    private String postAuthorID;
    private String avatarURL;

    public Post(String forumID, String topicID, String postID, String postTitle, String postText,
                String postAuthor, String postAuthorID, String postDate, String avatarURL) {
        this.postID = postID;
        this.postText = postText;
        this.postTitle = postTitle;
        this.postAuthor = postAuthor;
        this.postAuthorID = postAuthorID;
        this.postDate = postDate;
        this.type = ForumTypes.POST;
        this.forumID = forumID;
        this.topicID = topicID;
        this.avatarURL = avatarURL;
    }

    @Override
    public String getPostID() {
        return this.postID;
    }

    @Override
    public String getPostText() {
        return this.postText;
    }

    @Override
    public String getPostAuthor() {
        return this.postAuthor;
    }

    @Override
    public String getPostDate() {
        return this.postDate;
    }

    @Override
    public ForumTypes getType() {
        return this.type;
    }

    @Override
    public String getForumID() {
        return this.forumID;
    }

    @Override
    public String getTopicID() {
        return this.topicID;
    }

    @Override
    public String getPostTitle() {
        return this.postTitle;
    }

    @Override
    public String getPostAuthorID() {
        return this.postAuthorID;
    }

    @Override
    public String getAvatarURL() {
        return this.avatarURL;
    }

    @Override
    public String toString() {
        return "[Post: " + this.postAuthor + " | ID: " + this.postID + " | Text: " + this.postText + " ]";
    }
}
