package com.pipodi.italiansubsmobileclient.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Alex on 14/12/2015.
 */
public class NotificationService extends Service {

    public static final String SERVICE = "com.pipodi.italiansubsmobileclient.activities.NotificationService";
    public static final long REFRESH_INTERVAL = 300 * 1000;
    private List<String> notificationsPreferencesList = new ArrayList<>();
    private Timer timer = null;
    private Handler handler = new Handler();
    private int notificationID;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        this.notificationID = 0;
        Log.i("Service", "StartCommand");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Service", "onCreate");
        this.notificationID = 0;
        TempStorage.isServiceRunning = true;
        if (timer != null) {
            timer.cancel();
        } else {
            timer = new Timer();
        }
        String notificationsPreferences = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getString("NOTPREF", "Normale").replace(" ", "");
        String[] notificationArray = notificationsPreferences.split(",");
        for (String str : notificationArray) {
            notificationsPreferencesList.add(str);
        }
        timer.scheduleAtFixedRate(new MyItaSATimerTask(), 0, REFRESH_INTERVAL);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Service", "onDestroy");
        this.notificationID = 0;
        TempStorage.isServiceRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean checkInternetConnection(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    class MyItaSATimerTask extends TimerTask {

        @Override
        public void run() {
            if (checkInternetConnection(getApplicationContext()) && TempStorage.isServiceRunning) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        int latestID = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("LATESTID", 0);
                        Log.i("Service", "LatestID = " + latestID);
                        new AsyncTask<String, String, String>() {
                            @Override
                            protected String doInBackground(String... params) {
                                Document doc;
                                doc = Connection
                                        .connectToAPIURL("https://api.italiansubs.net/api/rest/myitasa/lastsubtitles?authcode="
                                                + getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getString("AUTHCODE", "")
                                                + "&apikey=ce0a811540244ad5267785731a5c37a0", getApplicationContext());
                                if (doc != null && doc.getContentSize() > 0 && LoginVariables.isRequestValid(doc)) {
                                    Iterator iter = XMLIterator.parseXMLTree(2, doc);
                                    if (iter.hasNext()) {
                                        Element item = (Element) iter.next();
                                        int sub_id = Integer.parseInt(item.getChild("id").getText());
                                        String sub_text = item.getChild("name").getText();
                                        String video_version = item.getChild("version").getText();
                                        int latestID = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("LATESTID", 0);
                                        Log.i("Service", "NewID = " + sub_id);
                                        if (latestID == 0) {
                                            getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit().putInt("LATESTID", sub_id).commit();
                                        } else if (sub_id != latestID && notificationsPreferencesList.contains(video_version)) {
                                            getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit().putInt("LATESTID", sub_id).commit();
                                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                                                    .setSmallIcon(R.drawable.ic_launcher2)
                                                    .setContentTitle("Nuovi sottotitoli disponibili!")
                                                    .setContentText(sub_text + " " + video_version)
                                                    .setAutoCancel(true)
                                                    .setDefaults(Notification.DEFAULT_ALL);
                                            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                                            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                            builder.setContentIntent(pendingIntent);
                                            NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                            mNotifyMgr.notify(1, builder.build());
                                        }
                                    }
                                }
                                return null;
                            }
                        }.execute("");
                    }
                });
            } else {
                Log.i("Notification", "No internet connection.");
            }
        }
    }
}
