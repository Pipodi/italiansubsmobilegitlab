package com.pipodi.italiansubsmobileclient.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.ConnectionForSubtitleNews;
import com.pipodi.italiansubsmobileclient.engine.MyItaSAUpdateConnection;
import com.pipodi.italiansubsmobileclient.engine.NewsDescriptionConnection;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

public class NewsViewerActivity extends AppCompatActivity {

    public static final int ADD_SHOW = 0;
    public static final int REMOVE_SHOW = 1;

    public static Activity activity;
    public static Context context;
    public static Typeface tf;
    private static SectionsPagerAdapter mSectionsPagerAdapter;
    private static ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int themeInt = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("THEME", 0);
        switch (themeInt) {
            case 0:
                setTheme(R.style.AppTheme_Base);
                break;
            case 1:
                setTheme(R.style.AppTheme_Base_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_viewer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("TITLE") + " " + getIntent().getStringExtra("EPISODE"));
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());
        context = this;
        activity = this;
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pagerNews);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        boolean withAds = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean("ADS", true);
        final AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setVisibility(View.GONE);
        if (withAds) {
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdView.setVisibility(View.VISIBLE);
                }
            });
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class DummySectionFragment extends Fragment {
        public static final String ARG_SECTION_NUMBER = "section_number";
        public static String[] episodes;
        public static String episodeString;
        public static String titleString;
        private boolean loaded = false;
        private Intent intentField;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public DummySectionFragment() {
            super();
        }

        @Override
        public View onCreateView(final LayoutInflater inflater,
                                 final ViewGroup container, Bundle savedInstanceState) {
            int position = getArguments().getInt(ARG_SECTION_NUMBER);
            final View view;
            this.intentField = activity.getIntent();
            if (intentField.getStringExtra("EPISODE") != null) {
                episodeString = intentField.getStringExtra("EPISODE");
            }
            if (intentField.getStringExtra("TITLE") != null) {
                titleString = intentField.getStringExtra("TITLE");
            }
            switch (position) {
                case 0:
                    view = inflater.inflate(R.layout.infos_news_viewer_layout, container, false);
                    TextView title = (TextView) view.findViewById(R.id.title_viewer);
                    TextView episode = (TextView) view.findViewById(R.id.episode_viewer);
                    TextView releaseDate = (TextView) view.findViewById(R.id.release_date_viewer);
                    ImageView photo = (ImageView) view.findViewById(R.id.photo_viewer);
                    TextView translators = (TextView) view.findViewById(R.id.translatorView);
                    TextView team = (TextView) view.findViewById(R.id.teamView);
                    TextView info = (TextView) view.findViewById(R.id.infoView);
                    TextView image = (TextView) view.findViewById(R.id.imageView);
                    TextView resync = (TextView) view.findViewById(R.id.resynchView);
                    TextView reviewer = (TextView) view.findViewById(R.id.reviewerView);
                    final ImageButton prefButton = (ImageButton) view.findViewById(R.id.pref_button);
                    final ImageButton infoButton = (ImageButton) view.findViewById(R.id.info_button);
                    int newsID = intentField.getIntExtra("NEWSID", TempStorage.currentNewsID);
                    if (!this.loaded) {
                        NewsDescriptionConnection descrConn = new NewsDescriptionConnection(
                                translators, team,
                                info, resync, image, reviewer, newsID, intentField);
                        descrConn.execute("");
                        this.loaded = true;
                    }
                    String photoURL = intentField.getStringExtra("PHOTOURL");
                    title.setText(intentField.getStringExtra("TITLE"));
                    episode.setText(intentField.getStringExtra("EPISODE"));
                    releaseDate.setText(intentField.getStringExtra("RELEASE"));
                    Glide.with(this).load(photoURL).asBitmap().into(photo);
                    final boolean[] isFavourite = {intentField.getBooleanExtra("FAVOURITE", false)};
                    if (isFavourite[0]) {
                        prefButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.pref_rem));
                    } else {
                        prefButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.pref_add));
                    }
                    prefButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (isFavourite[0]) {
                                MyItaSAUpdateConnection connection = new MyItaSAUpdateConnection(prefButton);
                                connection.execute(intentField.getIntExtra("ID", 0), REMOVE_SHOW);
                                isFavourite[0] = !isFavourite[0];
                            } else {
                                if (TempStorage.favouriteShows != null && TempStorage.favouriteShows.size() >= 99) {
                                    Toast.makeText(getActivity(), "Limite massimo di serie consentite raggiunto.", Toast.LENGTH_LONG).show();
                                } else {
                                    MyItaSAUpdateConnection connection = new MyItaSAUpdateConnection(prefButton);
                                    connection.execute(intentField.getIntExtra("ID", 0), ADD_SHOW);
                                    isFavourite[0] = !isFavourite[0];
                                }
                            }
                        }
                    });
                    infoButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(NewsViewerActivity.context, ShowInfoActivity.class);
                            intent.putExtra("ID", intentField.getIntExtra("ID", 0));
                            intent.putExtra("TITLE", intentField.getStringExtra("TITLE"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }
                    });
                    break;
                case 1:
                    view = inflater.inflate(R.layout.subtitles_news_viewer_layout, container, false);
                    ListView subsList = (ListView) view.findViewById(R.id.subtilesNewsList);
                    ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarSubs);
                    ConnectionForSubtitleNews connection = new ConnectionForSubtitleNews(titleString,
                            episodeString, subsList, inflater, progressBar);
                    connection.execute("");
                    break;
                default:
                    view = inflater.inflate(R.layout.fragment_dummy,
                            container, false);
                    return view;
            }
            return view;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new DummySectionFragment();
            Bundle args = new Bundle();
            args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "INFO";
                case 1:
                    return "SOTTOTITOLI";
            }
            return null;
        }
    }

}
