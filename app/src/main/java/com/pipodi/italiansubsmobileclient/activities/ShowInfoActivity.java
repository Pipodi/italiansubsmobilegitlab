package com.pipodi.italiansubsmobileclient.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.ShowInfoConnection;
import com.pipodi.italiansubsmobileclient.engine.ShowSubtitlesParser;
import com.pipodi.italiansubsmobileclient.utils.Constants;

import java.util.Locale;

public class ShowInfoActivity extends AppCompatActivity {

    public static Context context;
    private static Intent intent;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int themeInt = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("THEME", 0);
        switch (themeInt) {
            case 0:
                setTheme(R.style.AppTheme_Base);
                break;
            case 1:
                setTheme(R.style.AppTheme_Base_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());
        context = this;
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pagerNews);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        this.intent = getIntent();
        setTitle(intent.getStringExtra("TITLE"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class DummySectionFragment extends Fragment {
        public static final String ARG_SECTION_NUMBER = "section_number";
        private ShowInfoConnection showInfoConnection;
        private ProgressBar progressBarInfos;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public DummySectionFragment() {
            super();
        }

        @Override
        public View onCreateView(final LayoutInflater inflater,
                                 final ViewGroup container, Bundle savedInstanceState) {
            int position = getArguments().getInt(ARG_SECTION_NUMBER);
            View view;
            switch (position) {
                case 0:
                    view = inflater.inflate(R.layout.show_info_layout, container, false);
                    ImageView banner = (ImageView) view.findViewById(R.id.bannerImg);
                    TextView plot = (TextView) view.findViewById(R.id.plotText);
                    TextView genres = (TextView) view.findViewById(R.id.genresText);
                    TextView start = (TextView) view.findViewById(R.id.startDate);
                    TextView end = (TextView) view.findViewById(R.id.endDate);
                    TextView seasons = (TextView) view.findViewById(R.id.seasonsText);
                    TextView country = (TextView) view.findViewById(R.id.countryText);
                    TextView network = (TextView) view.findViewById(R.id.networkText);
                    TextView runtime = (TextView) view.findViewById(R.id.runtimeText);
                    TextView airdate = (TextView) view.findViewById(R.id.airdateText);
                    TextView airtime = (TextView) view.findViewById(R.id.airtimeText);
                    TextView lastepnum = (TextView) view.findViewById(R.id.lastEpNumText);
                    TextView lasteptitle = (TextView) view.findViewById(R.id.lastEpTitleText);
                    TextView lastepdate = (TextView) view.findViewById(R.id.lastEpDateText);
                    TextView nextepnum = (TextView) view.findViewById(R.id.nextEpNumText);
                    TextView nexteptitle = (TextView) view.findViewById(R.id.nextEpTitleText);
                    TextView nextepdate = (TextView) view.findViewById(R.id.nextEpDateText);
                    progressBarInfos = (ProgressBar) view.findViewById(R.id.loadingBar);
                    showInfoConnection = new ShowInfoConnection(intent.getIntExtra(("ID"), 0), banner, plot, genres,
                            start, end, seasons, country, network, runtime, airdate, airtime, lastepnum, lasteptitle, lastepdate, nextepnum,
                            nexteptitle, nextepdate, progressBarInfos, container);
                    showInfoConnection.execute("");
                    return view;
                case 1:
                    view = inflater.inflate(R.layout.subtitles_news_viewer_layout, container, false);
                    ListView subList = (ListView) view.findViewById(R.id.subtilesNewsList);
                    ProgressBar progressBarSubs = (ProgressBar) view.findViewById(R.id.progressBarSubs);
                    new ShowSubtitlesParser(context, intent.getStringExtra("TITLE"), intent.getIntExtra(("ID"), 0), progressBarSubs, subList, inflater).execute("");
                    return view;
                default:
                    view = inflater.inflate(R.layout.fragment_dummy,
                            container, false);
                    return view;
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new DummySectionFragment();
            Bundle args = new Bundle();
            args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "INFO";
                case 1:
                    return "SOTTOTITOLI";
            }
            return null;
        }
    }
}
